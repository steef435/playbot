# playbot - chat bot recording game results (WIP)

## Installation
First, create and enter a Python virtual environment using
```sh
python -m venv .venv
source .venv/bin/activate
```
Then install the (dev) dependencies using `pip install -r requirements.txt`.
After configuration, run the bot using `python playbot`.

## Configuration
Copy `example_config.json` to `config.json` and edit appropriately.
