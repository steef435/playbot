from typing import Final
from sqlalchemy import Boolean, Column, CheckConstraint, DateTime, ForeignKey, func, Integer, String
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import declarative_base, sessionmaker

engine = session = None # Created with init()
Base = declarative_base()

NICK_REGEX: Final = r"^[^\d\W]\w{0,29}$"

class Game(Base):
    __tablename__ = "game"

    id = Column(Integer, primary_key=True)
    nick = Column(String(31), unique=True) # ID used in chat (e.g. "catan")
    name = Column(String, nullable=False)  # Full name (e.g. "Kolonisten van Catan")

    CheckConstraint(nick.regexp_match(NICK_REGEX))

class Play(Base):
    __tablename__ = "play"

    id = Column(Integer, primary_key=True)
    game_id = Column(Integer, ForeignKey("game.id"), nullable=False)
    date = Column(DateTime, server_default=func.now())

class Player(Base):
    __tablename__ = "player"

    id = Column(Integer, primary_key=True)
    nick = Column(String(31), unique=True)  # This is also used in chat, so can/should be a nickname
    mxid = Column(String(255), unique=True) # Optional mxid to be used for chat, e.g. for "I won [...]"

    CheckConstraint(nick.regexp_match(NICK_REGEX))

class Seat(Base):
    __tablename__ = "seat"

    play_id = Column(Integer, ForeignKey("play.id", ondelete="CASCADE"), primary_key=True)
    player_id = Column(Integer, ForeignKey("player.id"), primary_key=True)
    points = Column(Integer)
    victory = Column(Boolean)

async def init(database: str, echo=False):
    global engine, session

    if engine is not None:
        return engine

    engine = create_async_engine(database, echo=echo)
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

    session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)
