import asyncio
import json
import signal

from nio import MatrixRoom, RoomMessageText

import commands as cmd
import commands.game
import commands.player
import database as db
import matrix as mx

CONFIG_FILE = "config.json"
client = None

async def message_callback(room: MatrixRoom, event: RoomMessageText) -> None:
    await cmd.parse(room, event)

def stop() -> None:
    for task in asyncio.all_tasks():
        task.cancel()

async def main(config: dict) -> None:
    await db.init(config["database"], echo=True)
    mx.init(config["homeserver"], config["user_id"], config["device_id"], config["access_token"])

    mx.client.add_event_callback(message_callback, RoomMessageText)

    await mx.client.sync_forever(timeout=30000) # TODO: sync token (from database?)

if __name__ == "__main__":
    with open(CONFIG_FILE, "r") as f:
        config = json.load(f)

    loop = asyncio.get_event_loop()
    for sig in signal.SIGINT, signal.SIGTERM:
        loop.add_signal_handler(sig, stop)
    loop.run_until_complete(main(config))
