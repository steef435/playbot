from commands import command
from commands.util import read_nick
import database as db
from database import Player
from matrix import send_error, send_info, send_success
from sqlalchemy import select, update
from sqlalchemy.exc import IntegrityError

@command("!iam", argc=1)
async def iam(room, text, args):
    """Usage: !iam <nick>"""

    nick = await read_nick(room, args[1])
    if nick is None: return
    mxid = text.sender

    async with db.session() as s:
        await s.execute(update(Player).where(Player.mxid==mxid).values(mxid=None))
        player = (await s.execute(select(Player).where(Player.nick==nick))).scalar()

        if player is None:
            await send_error(room, f'There is no player named "{nick}"!')
            return

        player.mxid = mxid
        await s.commit()

        await send_success(room, f'{nick} identified as {mxid}')

@command("!register", argc=1)
async def register(room, text, args):
    """Usage: !register <nick>"""

    nick = await read_nick(room, args[1])
    if nick is None: return

    async with db.session() as s:
        try:
            player = Player(nick=nick)
            s.add(player)
            await s.commit()
        except IntegrityError:
            await send_error(room, f'The nick "{nick}" is already in use!')
            return

        await send_success(room, f'Player {player.id} registered as "{nick}"')

@command("!whois", argc=1)
async def whois(room, text, args):
    """Usage: !whois <nick>"""

    nick = await read_nick(room, args[1])
    if nick is None: return

    async with db.session() as s:
        p = (await s.execute(select(Player).where(Player.nick==nick))).scalar()
        if p is None:
            await send_error(room, f'There is no player named "{nick}"!')
            return

        await send_info(room, f'Player {p.id}\n\tnick: {p.nick}\n\tmxid: {p.mxid}')
