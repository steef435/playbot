from commands import command
from commands.util import read_nick
import database as db
from database import Game
from matrix import send_error, send_info, send_success
from sqlalchemy import select, update
from sqlalchemy.exc import IntegrityError

@command("!newgame", argc=2)
async def newgame(room, text, args):
    """Usage: !newgame <nick> <full name>"""

    nick = await read_nick(room, args[1])
    if nick is None: return
    name = args[2]

    async with db.session() as s:
        try:
            game = Game(nick=nick, name=name)
            s.add(game)
            await s.commit()
        except IntegrityError:
            await send_error(room, f'The nick "{nick}" is already in use!')
            return

        await send_success(room, f'Game {game.id} registered as {nick} with full name {name}')

@command("!library")
async def game(room, text, args):
    """Display game library"""

    FSTR = "{:3d}. {:31s} {}"
    result = "Game library (id. nick name)"

    async with db.session() as s:
        games = (await s.execute(select(Game))).scalars()
        for g in games:
            result += '\n' + FSTR.format(g.id, g.nick, g.name)
        await send_info(room, result)
