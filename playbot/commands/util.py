from database import NICK_REGEX
from matrix import send_error
from nio import MatrixRoom
import re
import shlex
from typing import Optional

def split(s: str) -> list[str]:
    return shlex.split(s, comments=True, posix=True)

def valid_nick(nick: str) -> bool:
    return re.match(NICK_REGEX, nick) is not None

async def read_nick(room: MatrixRoom, nick: str) -> Optional[str]:
    if valid_nick(nick): return nick

    await send_error(room, f"{nick} is not a valid nick!")
    return None
