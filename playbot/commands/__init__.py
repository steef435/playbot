from abc import ABC, abstractmethod
from inspect import getdoc
from typing import Callable, Coroutine, Optional
from commands.util import split
from matrix import send_error
from nio import MatrixRoom, RoomMessageText

class _Command(ABC):
    @abstractmethod
    async def parse(self, room: MatrixRoom, event: RoomMessageText) -> bool:
        pass

commands: list[_Command] = []

SimpleCommand = Callable[[MatrixRoom, RoomMessageText, list[str]], Coroutine]
class _SimpleCommand(_Command):
    def __init__(self, name: str, command: SimpleCommand, argc: Optional[int] = None):
        self.name = name
        self.command = command
        self.argc = argc

    async def parse(self, room, event):
        parts = event.body.split()
        if len(parts) == 0 or parts[0] != self.name: return False

        try:
            args = split(event.body)
            l = len(args)

            if self.argc is not None and l - 1 != self.argc:
                await send_error(room, getdoc(self.command))
            else:
                await self.command(room, event, args)
        except ValueError as e:
            await send_error(room, f"Parse error: {e}")
        return True

def command(name: str, argc: Optional[int] = None) -> Callable[[SimpleCommand], SimpleCommand]:
    global commands
    def register(command: SimpleCommand) -> SimpleCommand:
        commands.append(_SimpleCommand(name, command, argc))
        return command
    return register

async def parse(room: MatrixRoom, event: RoomMessageText) -> None:
    global commands
    for c in commands:
        if await c.parse(room, event):
            return
