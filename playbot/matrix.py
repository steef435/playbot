import asyncio
from typing import Optional
from nio import AsyncClient, Event, MatrixRoom

client: AsyncClient = None # Created in init()

def init(homeserver: str, user_id: str, device_id: str, access_token: str):
    global client

    if client is not None:
        return

    client = AsyncClient(homeserver)
    client.user_id = user_id
    client.device_id = device_id
    client.access_token = access_token

async def send(room: MatrixRoom, body: str, reply_event: Optional[Event] = None) -> None:
    content: dict = {
        "msgtype": "m.notice",
        "body": body,
    }

    if reply_event is not None:
        content["m.relates_to"] = {
            "m.in_reply_to": {
                "event_id":  reply_event.event_id
            }
        }

    await client.room_send(
        room_id=room.room_id,
        message_type="m.room.message",
        content = content,
    )

async def send_error(room: MatrixRoom, body: str, reply_event: Optional[Event] = None):
    await send(room, '⚠ ' + body, reply_event)

async def send_info(room: MatrixRoom, body: str, reply_event: Optional[Event] = None):
    await send(room, 'ℹ️ ' + body, reply_event)

async def send_success(room: MatrixRoom, body: str, reply_event: Optional[Event] = None):
    await send(room, '✓ ' + body, reply_event)
